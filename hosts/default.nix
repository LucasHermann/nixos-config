{ config, pkgs, flake-inputs, ... }: {
  imports = [
    # flake-inputs.home-manager.nixosModules.home-manager
  ];

  nixpkgs.config = { allowUnfree = true; };
  nix = {
    package = pkgs.nixVersions.stable;

    settings = {
      auto-optimise-store = true;
      experimental-features = ["nix-command" "flakes"];
    };

    gc = {
      automatic = true;
      dates = "weekly";
    };

  # Make the nixpkgs flake input be used for various nix commands
  nixPath = ["nixpkgs=${flake-inputs.nixpkgs}"];
    registry.nixpkgs = {
      from = {
        id = "nixpkgs";
        type = "indirect";
      };
      flake = flake-inputs.nixpkgs;
    };
  };

  boot = {
    loader = {
      efi.canTouchEfiVariables = true;

      systemd-boot = {
        enable = true;
        editor = false;
      };
    };
  };

  networking = {
    useDHCP = false;
    useNetworkd = true;
  };

  # Disable Networkd waiting to connect as this is managed by NetowrkManager
  systemd.network.wait-online.enable = false;

  time.timeZone = "Europe/Paris";

  users = {
    defaultUserShell = pkgs.zsh;

    groups.network = {};

    users.lucas = {
      isNormalUser = true;
      extraGroups = ["wheel" "video" "network" "networkmanager"];
    };
  };

  environment.systemPackages = with pkgs; [
    arandr
    git
    pavucontrol
    vim
    whois
    wpa_supplicant_gui
    zoxide
  ];

  programs = {
    dconf.enable = true;
    zsh.enable = true;
  };

  fonts = {
    enableDefaultPackages = true;
    fontDir.enable = true;

    packages = with pkgs; [
      atkinson-hyperlegible
      atkinson-monolegible
      hack-font
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      font-awesome
    ];

    fontconfig = {
      defaultFonts = {
        serif = ["AtkinsonHyperlegible"];
        sansSerif = ["AtkinsonHyperlegible"];
        monospace = ["AtkinsonMonolegible"];
      };
    };
  };

  hardware = {
    bluetooth.enable = true;
    enableRedistributableFirmware = true;
  };

  security.rtkit.enable = true;

  system.stateVersion = "23.05";
  # home-manager.users.lucas.home.stateVersion = "23.05";
}
