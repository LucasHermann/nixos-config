# Configuration specific for AMO

{ config, pkgs, ... }:
let
  kubeMasterIP = "10.1.1.2";
  kubeMasterHostname = "api.kube";
  kubeMasterAPIServerPort = 443;
in
{
  imports = [
    ../hardware/lemongrab.nix
  ];

  # resolve master hostname
  networking.extraHosts = "${kubeMasterIP} ${kubeMasterHostname}";

  # packages for administration tasks
  environment.systemPackages = with pkgs; [
    kompose
    kubectl
    kubernetes
  ];

  # Enable Kubernetes
  services.kubernetes = {
    roles = ["master" "node"];
  };
}
