# Configuration specific for AMO

{ config, pkgs, lib, ...}:

let
  unfree-pkgs = config.unfree; # List of unfree pkgs
in
{
  imports = [
      ./hardware.nix
  ];

  # Allow unfree lincence for the pkgs in this list
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) unfree-pkgs;

  time.timeZone = "Europe/London";

  # Use the systemd-boot EFI boot loader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future.
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  # networking.interfaces.wlp0s20f3.useDHCP = true;
  networking.interfaces.wlan0.useDHCP = true;

  services.xserver.layout = "gb";
  services.xserver.xkbOptions = "eurosign:e";

  networking.hostName = "dmo";
  networking.networkmanager.enable = true;

  programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
  };

  networking.firewall.enable = false;

  #Add bluetooth support
  # hardware.bluetooth.enable = true;
  # services.blueman.enable = true;

  # Flakes requirement
  nix.package = pkgs.nixUnstable;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

}
