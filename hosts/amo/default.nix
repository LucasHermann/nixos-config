# Configuration specific for AMO

{ config, pkgs, lib, ...}:

let
  unfree-pkgs = config.unfree; # List of unfree pkgs
in
{
  imports = [
      ./hardware.nix
  ];

  # Allow unfree lincence for the pkgs in this list
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) unfree-pkgs;

  time.timeZone = "Europe/Paris";

  # Use the systemd-boot EFI boot loader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future.
  networking.useDHCP = false;
  networking.interfaces.enp39s0.useDHCP = true;

  services.xserver.xkb = {
    layout = "gb, us";
    variant = "intl, intl";
    options = "eurosign:e, grp:alt_space_toggle";
  };

  networking.hostName = "amo";
  networking.networkmanager.enable = true;

  programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
  };

  networking.firewall.enable = false;
}
