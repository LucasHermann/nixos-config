# Configuration specific for AMO

{ config, pkgs, lib, ...}:

let
  unfree-pkgs = config.unfree; # List of unfree pkgs
in
{
  imports = [
      ./hardware.nix
  ];

  # Allow unfree lincence for the pkgs in this list
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) unfree-pkgs;

  time.timeZone = "Europe/Paris";

  # Use the systemd-boot EFI boot loader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future.
  networking.useDHCP = false;
  # networking.interfaces.enp0s13f0u4u4.useDHCP = true;

  # services.xserver.layout = "gb";
  services.xserver.xkbOptions = "eurosign:e";

  networking.hostName = "tmo";
  networking.networkmanager.enable = true;

  programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
  };

  networking.firewall.enable = false;

  environment.systemPackages = with pkgs; [
    river
  ];

  # Flakes requirement
  nix.package = pkgs.nixUnstable;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # Replace lightdm with startx
  services.xserver.displayManager.startx.enable = true;

  # Disable display Manager auto start
  services.xserver.autorun = false;

}
