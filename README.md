# NixOS config

My NixOS configuration respository.

## Project Structure

The entry point of my configuration is `flake.nix`, this file conatins a list of flakes each matching to one possible system configuration.

The rest of my configuration is split into 2 categories:

1. hosts  
   Each `host` contains all the configuration specific to a single machine.
2. modules  
   Each `module` groups all the configuration required for a single functionality.  
   Currently I only have modules for a specific application, but in the future I expect to have module for more generic purpose such a `VPN` for example.

## TODO

- Manage secrets
- Start using this configuration acrsso multiple systems
- Try managing server configuration
  - No need for X server
- Get a Kubernetes server configuration

