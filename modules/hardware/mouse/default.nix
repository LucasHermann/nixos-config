# Module to configure Logitech Mouse

{ config, pkgs, ... }:

{
  # Required for piper
  services.ratbagd.enable = true;

  environment.systemPackages = with pkgs; [
    piper # GTK for gaming mouse configuration
  ];
}
