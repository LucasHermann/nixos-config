# Default configuration for Citrix

{ config, pkgs, lib, ...}:

{
  unfree = [
    "citrix_workspace"
  ];

  # When Updating make sure to download the matching version of the Citrix workspace application
  # It can be found here : https://www.citrix.com/downloads/workspace-app/
  # Once dwnloaded copy it to the module folder and add it to the local nix store via
  # nix-prefetch-url file://$PWD/modules/citrix/<tar-file-downloaded-from-citrix>
  environment.systemPackages = with pkgs; [
    citrix_workspace
  ];
}

