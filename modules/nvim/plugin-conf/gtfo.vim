" Gtfo configuration

" === Setup === "

" Configure got so it opens a new alacritty terminal
" Taken from issue : https://github.com/justinmk/vim-gtfo/issues/50
let g:gtfo#terminals = { 'unix': 'alacritty --working-directory $(pwd -P) &' }
