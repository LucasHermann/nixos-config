" Language server client
" Allow to use different language servers

" === Config === "
let g:LanguageClient_serverCommands = {
        \ 'rust': ['rust-analyzer'],
        \ }

" === Key Bindings === "
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
