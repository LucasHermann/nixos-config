" CtrlP Plugin Configuration File

" Custom commands for searching files
" Both are using Git and will use .gitignore
" First one was found here https://github.com/kien/ctrlp.vim/issues/174#issuecomment-49747252
" Second one is from the CtrlP readme https://github.com/ctrlpvim/ctrlp.vim#basic-options
let g:ctrlp_user_command = {
	\ 'types': {
		\ 1: ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard'],
		\ 2: ['.git', 'cd %s && git ls-files'],
		\ },
	\ 'fallback': 'find %s -type f'
	\ }

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" === Key Bindings === "

" Search accros the current folder
nmap <leader>p :CtrlP<cr>

" Search accros the buffers currently open
nmap <leader>; :CtrlPBuffer<cr>

" Search accros the Most Recently Used
nmap <leader>[ :CtrlPMRU<cr>

" Search accros all combined
nmap <leader>] :CtrlPMixed<cr>
