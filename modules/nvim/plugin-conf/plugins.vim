" Plug Plugins "
" Thid file lists all the plugins installed via Plug

" Check whether vim-plug is installed and install it if necessary
let plugpath = expand('<sfile>:p:h'). '/autoload/plug.vim'
if !filereadable(plugpath)
    if executable('curl')
        let plugurl = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
        call system('curl -fLo ' . shellescape(plugpath) . ' --create-dirs ' . plugurl)
        if v:shell_error
            echom "Error downloading vim-plug. Please install it manually.\n"
            exit
        endif
    else
        echom "vim-plug not installed. Please install it manually or install curl.\n"
        exit
    endif
endif

call plug#begin('~/.local/share/nvim/plugged')


" === Editing Plugins === "

" Trailing whitespace highlighting & automatic fixing
Plug 'ntpeters/vim-better-whitespace'

" Simple easy to use Vim alignment plugin
Plug 'junegunn/vim-easy-align'

" Fold plugin for python
Plug 'tmhedberg/SimpylFold', {'for': 'python'}

" En/Disable comment
Plug 'scrooloose/nerdcommenter'

" Automatically create closing quote/bracket
Plug 'jiangmiao/auto-pairs'

" Easy alignment
Plug 'junegunn/vim-easy-align'

" Client for Language Server
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" Full Path Finder
Plug 'ctrlpvim/ctrlp.vim'

" Run Command Asyncrhonously
Plug 'skywind3000/asyncrun.vim'


" === UI Plugins === "

" Colorscheme
Plug 'mhartington/oceanic-next'

" Customized vim status line
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" File explorer
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}

" Visual indentation lines
Plug 'Yggdroot/indentLine'

call plug#end()
