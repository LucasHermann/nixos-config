" Easy Align configuration

" === Key bindings === "

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

