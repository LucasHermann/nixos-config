# Neovim configuration

{ config, lib, pkgs, ... }:

let
  vimConfig = ''
    " Hides buffers instead of closing itn
    set hidden

    set number
    set signcolumn=number
    set expandtab

    :colorscheme carbonfox

    " Configure indetation per FileType
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2
    autocmd FileType nix setlocal ts=2 sts=2 sw=2
    autocmd FileType markdown setlocal ts=2 sts=2 sw=2
    autocmd FileType plantuml setlocal ts=2 sts=2 sw=2
    autocmd FileType lua setlocal ts=3 sts=3 sw=3
    autocmd FileType erlang setlocal ts=4 sts=4 sw=4

    " Command to format markdown table
    xnoremap <leader>ft :!tr -s " " <bar> column -t -s '<bar>' -o '<bar>'

    lua require("init")
  '';

  customTelescope = {
    plugin = pkgs.vimPlugins.telescope-nvim;
    config = ''
      packadd! telescope.nvim
      lua << END
      local builtin = require('telescope.builtin')
      vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
      vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
      vim.keymap.set('n', '<leader>fb', builtin.buffers, {})

      vim.keymap.set('n', '<leader>ld', builtin.lsp_definitions, {})
      vim.keymap.set('n', '<leader>lr', builtin.lsp_references, {})
      END
    '';
  };

  customTypstPreview = {
    plugin = pkgs.vimPlugins.typst-preview-nvim;
    config = ''
      noremap <leader>tt :TypstPreview<cr>
    '';
  };

  telescopeWithPlugins = [
    customTelescope
    (pkgs.vimPlugins.nvim-treesitter.withPlugins (p: [
      p.c
      p.erlang
      p.gitcommit
      p.gitignore
      p.javascript
      p.json
      p.lua
      p.mermaid
      p.typescript
      p.typst
      p.nix
      p.python
      p.rust
      p.yaml
    ]))
  ];

  luaConfig = pkgs.vimUtils.buildVimPlugin {
    name = "my-config";
    src = builtins.path { path = ./nvim-config; name = "luaConfig"; };
   };

  lspClients = builtins.attrValues {
    inherit (pkgs)
    erlang-ls
    lua-language-server
    pyright
    rust-analyzer
    yaml-language-server;
  };

in
{
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    configure = {
      customRC = vimConfig;
      packages.myVimPackage = {
        start = [
          luaConfig

          customTypstPreview
          
          pkgs.vimPlugins.nvim-cmp
          pkgs.vimPlugins.cmp-buffer
          pkgs.vimPlugins.cmp-path
          pkgs.vimPlugins.cmp-treesitter
          pkgs.vimPlugins.cmp-nvim-lsp
          pkgs.vimPlugins.cmp_luasnip

          pkgs.vimPlugins.nvim-lspconfig
          pkgs.vimPlugins.luasnip

          pkgs.vimPlugins.nightfox-nvim
        ]
        ++ telescopeWithPlugins;
      };
    };
  };

  environment.systemPackages = [
    pkgs.nodePackages_latest.typescript-language-server
    pkgs.tinymist
  ] ++
  lspClients;
}
