# Zellij Default Configuration

{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    zellij
  ];
}

