# Default configuration for Discord

{ config, pkgs, lib, ...}:

{
  unfree = [
    "discord"
  ];

  environment.systemPackages = with pkgs; [
    discord
  ];
}
