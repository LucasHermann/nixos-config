# Pass Default Configuration

{ config, pkgs, libs, ...}:

let
  custom-pass = pkgs.pass.withExtensions ({ ... } @ exts: [exts.pass-tomb]);
in
{
  imports = [
    ../gpg/default.nix
  ];

  environment.systemPackages = [
    custom-pass
    pkgs.pass-git-helper
  ];
}

