# Yubikey Default Configuration

{ config, pkgs, lib, ... }:

{
  imports = [
    ../gpg/default.nix
  ];

  # Daemon to allow talking to SmartCards
  services.pcscd.enable = true;

  environment.systemPackages = with pkgs; [
    yubikey-personalization
    yubikey-manager
  ];

  # Allow udev to access the Yubikey over USB
  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];

  #temp test for ssh-config
  # home-manager.users.${config.username}.programs.ssh = {
  #   enable = true;
  #   matchBlocks = {
  #     "devhw13.guest.codethink.co.uk" = {
  #       hostname = "devhw13.guest.codethink.co.uk";
  #       port = 20022;
  #       user = "local-lucashermann";
  #     };
  #   };
  # };

  # User Auth via Yubikey using open standard
  security.pam.services = {
    sudo.u2fAuth = true;
    login.u2fAuth = false;
  };
  # User loggin via Yubikey
  # security.pam.yubico = {
   # enable = true;
   # mode = "challenge-response";
   # control = "required"; # 2FA Password and Yubikey
   # debug = true;
 # };
}

