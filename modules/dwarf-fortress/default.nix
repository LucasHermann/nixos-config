# Dwarf Fortress

{ config, lib, pkgs, ... }:

let
  themes = pkgs.dwarf-fortress-packages.themes;
  custom-df = pkgs.dwarf-fortress.override {
    enableSound = false;
    enableIntro = false;
    theme = themes.spacefox;
  };
in
{
  unfree= [
    "dwarf-fortress"
  ];

  environment.systemPackages = with pkgs; [
    custom-df
  ];

}

