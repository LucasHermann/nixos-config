# Zoxide configuration file

{ config, pkgs, lib, ...}:

{
  environment.systemPackages = with pkgs; [
    zoxide
  ];
}

