# Tomb Default Configuration

{ config, pkgs, lib, ... }:

{
  imports = [
    ../gpg/default.nix
  ];

  environment.systemPackages = with pkgs; [
    lsof
    tomb
  ];

}

