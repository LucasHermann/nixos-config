# Default configuration for Citrix

{ config, pkgs, lib, ...}:

{
  environment.systemPackages = with pkgs; [
    awscli2
    aws-adfs
  ];
}

