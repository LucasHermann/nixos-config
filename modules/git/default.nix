# Git Configuration file

{ config, ...}:

{
  programs.git = {
    enable = true;
    config = {
      user = {
        name = "${config.name}";
        email = "${config.email}";
      };

      init = {
        defaultBranch = "trunk";
      };

      aliases = {
        gl = "log --oneline --graph";
      };

      # Matches from top to bottom of the list
      includeIf = {
        "gitdir:~/Work/" = {
          path = "~/Work/gitconfig";
        };
        "gitdir:~/Work/Missions/AllianzTrade/" = {
          path = "~/Work/Missions/AllianzTrade/gitconfig";
        };
      };

      push = {
        default = "simple";
        autoSetupRemote = true;
      };

      branch.sort = "-committerdate";
      tag.sort = "version:refname";

      diff = {
        algorithm = "histogram";
        colorMoved = "plain";
        mnemonicPrefix = true;
        renames = true;
      };

      help.autocorrect = "prompt";

      commit.verbose = true;

      rerere = {
        enabled = true;
        autoupdate = true;
      };

      rebase = {
        autoSquash = true;
        autoStash = true;
        updateRefs = true;
      };

    };
  };
}
