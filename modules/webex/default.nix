# Default configuration for Webex

{ config, pkgs, lib, ...}:

{

  environment.systemPackages = with pkgs; [
    webex
  ];
}

