# Core configuration shared by all my systems

{ config, pkgs, ... }:

{
  imports = [
    ./globals.nix
  ];

  # System wide pkgs
  environment.systemPackages = with pkgs; [
    alacritty
    arandr
    feh
    fd
    firefox
    jq
    git
    htop
    btop
    ripgrep
    tree
    vim
    wget
    xclip
    broot
    hacksaw
    shotgun
    unzip
    visidata
    zip
  ];

  programs = {
    ${config.shell}.enable = true;
    direnv.enable = true;
  };

  # users.users.${config.username} = {
  #   isNormalUser = true;
  #   createHome = true;
  #   home = "/home/${config.username}";
  #   extraGroups = ["audio" "networkmanager" "wheel"];
  #   shell = pkgs.${config.shell};
  # };

  system.stateVersion = "23.05";
}

