# This files contains setting values shared accross different configurations
{ config, pkgs, lib, ...}:

with lib;
{
  options = {
    name = mkOption {
      default = "Lucas Hermann";
      type = with types; uniq str;
    };
    username = mkOption {
      default = "lucas";
      type = with types; uniq str;
    };
    email = mkOption {
      default = "herm.lucas@gmail.com";
      type = with types; uniq str;
    };
    terminal = mkOption {
      default = "alacritty";
      type = with types; uniq str;
    };
    shell = mkOption {
      default = "zsh";
      type = with types; uniq str;
    };
    env = mkOption {
      default = "";
      type = with types; uniq str;
    };
    profile = mkOption {
      default = "lucas";
      type = with types; uniq str;
      description = ''
        Profiles are a higher level grouping than hosts.
        They can be useful to group related things (e.g. ssh-keys)
        that should be available on multiple hosts
      '';
    };
    keyboardLayout = mkOption {
      type = with types; uniq str;
      description= ''
      '';
    };
    unfree = mkOption {
      type = with types; listOf str;
      description = "List of packages that aren't under a free licence";
      default = [];
    };
  };
}


