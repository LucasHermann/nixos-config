# Default configuration for Wifi using iwd

{ config, pkgs, lib, ... }:

{
  # networking.networkmanager.wifi.backend = "iwd";
  # networking.wireless.iwd.enable = true;

  environment.systemPackages = with pkgs; [
    wpa_supplicant
  ];

  programs.nm-applet.enable = true;

  networking = {
    networkmanager.enable = true;
    # wireless = {
    #   enable = true;
    #   userControlled.enable = true;
    #   environmentFile = "/run/secrets/wireless.env";

    #   networks = {
    #     "a2_wpa2auth" = {
    #       auth = ''
    #         key_mgmt=WPA-EAP
    #         eap=PEAP
    #         phase2="auth=MSCHAPV2"
    #         identity="1369855@az.wlan"
    #         password="@PASS_WORK@"
    #         ca_cert="/home/lucas/Downloads/rootca3_base64.cer"
    #       '';
    #     };
    #   };
    # };
  };
}


