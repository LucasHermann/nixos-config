{ config, pkgs, options, ... }:

{
  environment.systemPackages = with pkgs; [
    pavucontrol
    wireplumber
  ];

  # Disable previous sound's setting
  sound.enable = false;

  # High quality BT calls
  hardware.bluetooth.enable = true;
  hardware.bluetooth.hsphfpd.enable = false;

  # Pipewire config
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    # No idea if I need this
    alsa.support32Bit = true;
    pulse.enable = true;

    wireplumber.extraConfig.bluetoothEnhancements = {
      "monitor.bluez.properties" = {
          "bluez5.enable-sbc-xq" = true;
          "bluez5.enable-msbc" = true;
          "bluez5.enable-hw-volume" = true;
          "bluez5.roles" = [ "hsp_hs" "hsp_ag" "hfp_hf" "hfp_ag" ];
      };
    };
  };
}
