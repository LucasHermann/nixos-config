# Nixos system configuration for i3

{ config, pkgs, ...}:

{
  ## Xorg configuration
  services = {
    displayManager = {
      defaultSession = "none+i3";
    };
    xserver = {
      enable = true;

      desktopManager = {
        xterm.enable = false;
      };

    displayManager.lightdm.enable = true;

      windowManager.i3 = {
        enable = true;
        configFile = ./config;
        extraPackages = with pkgs; [
          dmenu
          i3status
          i3lock
        ];
      };
    };
    };
}

