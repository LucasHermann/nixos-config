# Home-manager configuration for i3

{ config, pkgs, lib, ...}:

with lib;
let
  mod = "Mod4";
  WS1 = "1";
  WS2 = "2 ";
  WS3 = "3 ";
  WS4 = "4";
  WS5 = "5";
  WS6 = "6";
  WS7 = "7";
  WS8 = "8 ";
  WS9 = "9 ";
  WS10 = "10";
in
{
  home-manager.users.${config.username}.xsession.windowManager.i3 = {
    enable = true;
    config = {
      modifier = mod;
      # Application to workspace assignment block
      assigns = {
        "${WS3}" = [{class="^Firefox$";}];
        "${WS8}" = [{class="Steam";}];
        "${WS9}" = [{class="discord";}];
      };
      # Keybindings configuration block
      keybindings = {
        # Workspace keybindings
        "${mod}+1" = "workspace ${WS1}";
        "${mod}+2" = "workspace ${WS2}";
        "${mod}+3" = "workspace ${WS3}";
        "${mod}+4" = "workspace ${WS4}";
        "${mod}+5" = "workspace ${WS5}";
        "${mod}+6" = "workspace ${WS6}";
        "${mod}+7" = "workspace ${WS7}";
        "${mod}+8" = "workspace ${WS8}";
        "${mod}+9" = "workspace ${WS9}";
        "${mod}+0" = "workspace ${WS10}";

        "${mod}+Shift+1" = "move container to workspace ${WS1}";
        "${mod}+Shift+2" = "move container to workspace ${WS2}";
        "${mod}+Shift+3" = "move container to workspace ${WS3}";
        "${mod}+Shift+4" = "move container to workspace ${WS4}";
        "${mod}+Shift+5" = "move container to workspace ${WS5}";
        "${mod}+Shift+6" = "move container to workspace ${WS6}";
        "${mod}+Shift+7" = "move container to workspace ${WS7}";
        "${mod}+Shift+8" = "move container to workspace ${WS8}";
        "${mod}+Shift+9" = "move container to workspace ${WS9}";
        "${mod}+Shift+0" = "move container to workspace ${WS10}";

        "${mod}+o" = "mode move_workspace"; # Special mode for multi monitors
        "${mod}+f" = "fullscreen toggle";

        "${mod}+d" = "exec ${pkgs.dmenu}/bin/dmenu_run";
        "${mod}+Shift+c" = "reload";
        "${mod}+Shift+r" = "restart";
        "${mod}+Shift+e" = "exec i3-nagbar -t warning -m 'Do you want to exit i3?' -b 'Yes' 'i3-msg exit'";
        "${mod}+Shift+q" = "kill";
        "${mod}+Return" = "exec ${config.terminal}";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";
        "${mod}+h" = "focus left";
        "${mod}+u" = "focus parent";
        "${mod}+Shift+U" = "focus child";
        "${mod}+Shift+J" = "move down";
        "${mod}+Shift+K" = "move up";
        "${mod}+Shift+L" = "move right";
        "${mod}+Shift+H" = "move left";
        "${mod}+v" = "split v";
        "${mod}+semicolon" = "split h";
        "${mod}+space" = "layout toggle splitv splith tabbed";
        "${mod}+y" = "bar mode toggle";
        "${mod}+Shift+N" = "exec \"xterm -e 'sudo nixos-rebuild switch; read -s -k \\?COMPLETE'\"";
        "${mod}+Control+L" = "exec i3lock";
        #Passmenu key bindings
        "${mod}+p" = "exec passmenu";
      };
      modes = {
        resize = {
          "h" = "resize shrink width 10 px or 10 ppt";
          "j" = "resize grow height 10 px or 10 ppt";
          "k" = "resize shrink height 10 px or 10 ppt";
          "l" = "resize grow width 10 px or 10 ppt";
          "Escape" = "mode default";
          "Return" = "mode default";
        };
        move_workspace = {
          "h" = "move workspace to output left";
          "j" = "move workspace to output down";
          "k" = "move workspace to output up";
          "l" = "move workspace to output right";
          "Escape" = "mode default";
        };
      };
      window.titlebar = false;
    };
  };
}

