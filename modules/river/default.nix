{ config, pkgs, ...}:

let
  river-config = pkgs.writeText "init" ''
  '';

  wrapped-river = pkgs.symlinkJoin {
    name = "river";
    paths = [ pkgs.unstable.bemenu ];
    buildInputs = [ pkgs.makeWrapper ];
    postBuild = ''
      '';
  };

  custom-bemenu = pkgs.unstable.bemenu.override {
    ncursesSupport = false;
    x11Support = false;
  };

  wrapped-bemenu = pkgs.symlinkJoin {
    name = "wrapped-bemenu";
    paths = [ custom-bemenu ];
    buildInputs = [ pkgs.makeWrapper ];
    postBuild = ''
      wrapProgram $out/bin/bemenu \
                  --set BEMENU_BACKEND wayland \
                  --add-flags "--binding vim" \

      wrapProgram $out/bin/bemenu-run \
                  --set BEMENU_BACKEND wayland \
                  --add-flags "--binding vim" \
    '';
  };
in
{
  environment.systemPackages = with pkgs; [
    wrapped-bemenu
    foot
    firefox-wayland
    unstable.river
    wayland
    wl-clipboard
    xdg-desktop-portal-wlr
    xdg-desktop-portal-gtk
  ];

  services.pipewire.enable = true;

  xdg = {
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr
        xdg-desktop-portal-gtk
      ];
    };
  };

  environment.sessionVariables = {
    MOZ_ENABLE_WAYLAND = "1";
  };
}

