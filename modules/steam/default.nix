# Steam configuration

{ config, lib, pkgs, ... }:

{
  unfree= [
    "steam"
    "steam-original"
    "steam-runtime"
  ];

  programs.steam.enable = true;

  #Kernel arguments for AMDGPU
  boot.kernelParams = [
    "amdgpu.dpm=0"
    "amdgpu.aspm=0"
    "amdgpu.runpm=0"
    "amdgpu.bapm=0"
  ];

  # Kernel driver for Xbox one controllers
  hardware.xone.enable = true;
}
