# Zsh configuration file

{ config, pkgs, lib, ...}:

{
  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    enableCompletion = true;
    # dotDir = ".config/zsh";
    ohMyZsh = {
      enable = true;
      theme = "lambda";
      plugins = [
        "git"
        "sudo"
        # {
        #   name = "zsh-nix-shell";
        #   file = "nix-shell.plugin.zsh";
        #   src = pkgs.fetchFromGitHub {
        #     owner = "chisui";
        #     repo = "zsh-nix-shell";
        #     rev = "v0.1.0";
        #     sha256 = "0snhch9hfy83d4amkyxx33izvkhbwmindy0zjjk28hih1a9l2jmx";
        #   };
        # }
      ];
    };
    shellAliases = {
      "ll" = "ls -al";
      "ns" = "nix-shell --command zsh";
    };
    shellInit = ''
      # Set the keybinding to vim
      bindkey -v
      eval "$(direnv hook zsh)"
    '';
  };

  environment.sessionVariables = {
    EDITOR = "nvim";
    TERM ="${config.terminal}";
    TERMINAL ="${config.terminal}";
    ZDOTFIR = "~/.config/zsh";
  };
}
