# Default configuration for GnuPG

{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    pinentry-curses
    openpgp-card-tools
  ];

  # Disable ssh-agent
  programs.ssh.startAgent = false;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryPackage = pkgs.pinentry-curses;
  };
}

