{
  description = "Lucas' NixOs definitions";

  inputs = {
    # Flake of the current stable version of NixOs
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";

    # Flake of the unstable channel of NixOs
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    # Hardware specific derivations
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    # Temporary change, wait for PR https://github.com/NixOS/nixos-hardware/pull/511
    # nixos-hardware.url = "github:mexisme/nixos-hardware/microsoft/surface/kernel-6.0.11";

    # Flake of the different tools for flake
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, nixos-hardware, ... } @ inputs:
  {
    nixosConfigurations = {
      amo = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./hosts
          ./hosts/amo
          ./modules/core
          ./modules/discord
          ./modules/git
          ./modules/hardware/mouse
          ./modules/i3
          ./modules/nvim
          ./modules/pass
          ./modules/podman
          ./modules/steam
          ./modules/tomb
          ./modules/yubikey
          ./modules/zsh
        ];

        specialArgs.flake-inputs = inputs;
      };

#       cmo = make-nixos-system {
#         nixpkgs = inputs.nixpkgs;
#         system = "x86_64-linux";
#         modules = [
#           (import ./modules/core)
#           (import ./hosts/cmo)
#           (import ./modules/git)
#           (import ./modules/i3)
#           (import ./modules/nvim)
#           (import ./modules/tomb)
#           (import ./modules/pass)
#           (import ./modules/podman)
#           (import ./modules/yubikey)
#           (import ./modules/zellij)
#           (import ./modules/zsh)
#         ];
#       };
# 
#       tmo = make-nixos-system {
#         nixpkgs = inputs.nixpkgs;
#         system = "x86_64-linux";
#         modules = [
#           inputs.nixos-hardware.nixosModules.microsoft-surface-pro-intel
#           (import ./modules/core)
#           (import ./hosts/tmo)
#           (import ./modules/bluetooth)
#           (import ./modules/dwarf-fortress)
#           (import ./modules/git)
#           (import ./modules/i3)
#           (import ./modules/nvim)
#           (import ./modules/pass)
#           # (import ./modules/podman)
#           (import ./modules/river)
#           (import ./modules/sound)
#           (import ./modules/tomb)
#           (import ./modules/wifi)
#           (import ./modules/yubikey)
#           (import ./modules/zoxide)
#           (import ./modules/zsh)
#         ];
#       };

      fmo = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./hosts
          ./hosts/fmo
          ./modules/core
          ./modules/discord
          ./modules/aws
          ./modules/citrix
          ./modules/bluetooth
          ./modules/podman
          ./modules/git
          ./modules/i3
          ./modules/nvim
          ./modules/pass
          ./modules/printer
          ./modules/slack
          ./modules/steam
          ./modules/tomb
          ./modules/webex
          ./modules/wifi
          ./modules/yubikey
          ./modules/zoxide
          ./modules/zsh
        ];

        specialArgs.flake-inputs = inputs;
      };
    };
  };
}
